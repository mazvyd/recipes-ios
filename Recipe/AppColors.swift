//
//  AppColors.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// An extension class for UIColor to be able to use own colors in this app, and be able to manage all of them from one class.
extension UIColor {

    /**
     Helper method to easily create an RGB color

     - parameter r: red
     - parameter g: green
     - parameter b: blue

     - returns: color
     */
    class func rgbColor(red r: Float, green g: Float, blue b: Float) -> UIColor {
        return UIColor(red: CGFloat(r/255.0), green: CGFloat(g/255.0), blue: CGFloat(b/255.0), alpha: 1.0)
    }

}
