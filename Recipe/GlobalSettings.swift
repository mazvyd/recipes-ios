//
//  GlobalSettings.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import Foundation
import UIKit

// Customisation colors
 /// Tint color that is used throughout the whole app
let globalTintColor = UIColor.rgbColor(red: 234.0, green: 41.0, blue: 125.0)

let tabbarIconTintColor = UIColor.rgbColor(red: 234.0, green: 41.0, blue: 125.0)
let tabbarBackgroundColor = UIColor.rgbColor(red: 252.0, green: 250.0, blue: 251.0)

 /// Navigation bar color
let navigationBarColor = UIColor.rgbColor(red: 252.0, green: 250.0, blue: 251.0)

// MARK: AdMob keys
let adMobAdUnitID = "ca-app-pub-5355260485883536/3391485601"
let adMobInterstitialAdUnitID = "ca-app-pub-5355260485883536/"


// MARK: Backendless.com keys
/// Get these keys from backendless.com and specify them here (On backendless.com, Manage > App Settings)
let backendlessAPPID = "8DD3BA05-8912-A788-FFCE-717CFB25B400"
let backendlessSecretKey = "FFEC9F97-FC10-3844-FF9E-A4BE1AF7CE00"
let backendlessVersionKey = "v1"

/// When set to false, it will download Recipes and Collections from your server. Otherwise, it will use all your data from the RecipeCollections folder
let useLocalStore = true
