//
//  AppDelegate.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit
import Firebase

/// AppDelegate
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    /// Window of the app
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        FIRApp.configure()

        return true
    }

    /**
     Save the favorites locally when the app goes to the background

     - parameter application: application
     */
    func applicationDidEnterBackground(_ application: UIApplication) {
        FavoritesManager.sharedInstance.saveFavoritesToDisk()
    }

    /**
     When the app becomes active, load the favorites from the disk

     - parameter application: application
     */
    func applicationDidBecomeActive(_ application: UIApplication) {
        FavoritesManager.sharedInstance.readFavoritesFromDisk()
    }

}

