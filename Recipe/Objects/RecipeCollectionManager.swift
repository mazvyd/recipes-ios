//
//  RecipeCollectionManager.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Manager object that reads out all the images and collections from the local folder, and copies them to another one
class RecipeCollectionManager: NSObject {
    
    /// Name of the folder where all the recipes and collections are saved into
    private let recipeCollectionsFolderName = "RecipeCollections"
    
    /// Array of RecipeCollection
    private var recipeCollectionsArray: [RecipeCollection] = []
    
    private let backendlessDownloader: BackendlessDownloader = BackendlessDownloader()
    
    /**
     Returns the loaded recipe collections
     
     - returns: recipe collections
     */
    func recipeCollections() -> [RecipeCollection] {
        return recipeCollectionsArray
    }
    
    override init() {
        super.init()
        if useLocalStore == false {
            let backendless = Backendless.sharedInstance()
            backendless?.initApp(backendlessAPPID, secret: backendlessSecretKey, version: backendlessVersionKey)
        }
    }
    
    /**
     Read all the recipes and collections from our main folder
     */
    func readRecipeCollectionsFolder(completionBlock: Optional<(_ collections: [RecipeCollection]?, _ error: String?) -> ()>) {
        if useLocalStore == true {
            do {
                let recipeCollectionsFolder: NSString = recipeCollectionsFolderPath()
                let itemsInRecipeCollectionsFolder = try FileManager.default.contentsOfDirectory(atPath: recipeCollectionsFolder as String)
                
                if itemsInRecipeCollectionsFolder.count > 0 {
                    for collection in itemsInRecipeCollectionsFolder {
                        let recipeFolderFullPath: String = recipeCollectionsFolder.appendingPathComponent(collection)
                        print(recipeFolderFullPath)
                        let recipeCollection = readContentOfFolderAtPath(path: recipeFolderFullPath as NSString)
                        recipeCollectionsArray.append(recipeCollection)
                    }
                }
                if let completionBlock = completionBlock {
                    completionBlock(recipeCollectionsArray, nil)
                }
            } catch let error as NSError {
                // failed to read directory
                print("Couldn't read recipe collection directory: \(error.localizedDescription)")
                if let completionBlock = completionBlock {
                    completionBlock(recipeCollectionsArray, error.localizedDescription)
                }
            }
        } else {
            backendlessDownloader.downloadCollections(completion: { (collections, errorMessage) in
                if let completionBlock = completionBlock {
                    if let collections = collections {
                        self.recipeCollectionsArray = collections
                    }
                    completionBlock(collections, errorMessage)
                }
            })
        }
    }
    
    /**
     Reads the content of a path and returns a RecipeCollection
     
     - parameter path: path to read
     
     - returns: RecipeCollection object
     */
    private func readContentOfFolderAtPath(path: NSString) -> RecipeCollection {
        var recipes: [Recipe] = []
        var recipeCollectionMainImagePath: String = ""
        
        if isFileDirectory(path: path as String) {
            // Get the images
            let recipesFolder = try! FileManager.default.contentsOfDirectory(atPath: path as String)
            for singleRecipeFolder in recipesFolder {
                let fullFilePath: NSString = path.appendingPathComponent(singleRecipeFolder) as NSString
                if isFileDirectory(path: fullFilePath as String) {
                    let recipe = readContentsOfFolderAtPath(recipeFullPath: fullFilePath)
                    recipes.append(recipe)
                } else {
                    // If it's not a directory, than it must be the image for the Recipe Collection
                    recipeCollectionMainImagePath = fullFilePath as String
                }
            }
        }
        
        return RecipeCollection(withImagePath: recipeCollectionMainImagePath, folderPath: path as String, recipes: recipes)
    }
    
    /**
     Reads the content of a folder at a given path and transforms it into a Recipe object
     
     - parameter path: path to read
     
     - returns: recipe object
     */
    private func readContentsOfFolderAtPath(recipeFullPath: NSString) -> Recipe {
        var recipeJSONPath = ""
        var recipeImagePath = ""
        
        let recipeFolderContent: [NSString] = try! FileManager.default.contentsOfDirectory(atPath: recipeFullPath as String) as [NSString]
        for filePath in recipeFolderContent {
            // Parse all the content here and create a Recipe object
            let fileFullPath = recipeFullPath.appendingPathComponent(filePath as String)
            let image = UIImage(contentsOfFile: fileFullPath)
            if image != nil {
                recipeImagePath = fileFullPath as String // this file the image of the Recipe
            } else {
                recipeJSONPath = fileFullPath as String // this file is the json.
            }
        }
        
        return Recipe(jsonFilePath: recipeJSONPath, mainImagePath: recipeImagePath)
    }
    
    /**
     Returns true if the given path is a directory
     
     - parameter path: path to check
     
     - returns: true if it is a directory
     */
    private func isFileDirectory(path: String) -> Bool {
        var isDirectory: ObjCBool = ObjCBool(false)
        
        let exists = FileManager.default.fileExists(atPath: path, isDirectory: &isDirectory)
        return (exists == true) && isDirectory.boolValue
    }
    
    /**
     The local path of the Recipe folder where we copy all the recipes
     
     - returns: folder path
     */
    private func recipeCollectionsFolderPath() -> NSString {
        let path: NSString = Bundle.main.resourcePath! as NSString
        return path.appendingPathComponent(recipeCollectionsFolderName) as NSString
    }
    
}

