//
//  RecipesDatasource.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

class RecipesDatasource: NSObject, UICollectionViewDataSource, UISearchBarDelegate {
    
    var selectedCollection: RecipeCollection!
    weak var collectionView: UICollectionView?
    
    /// Recipes that are being filtered with the searchterm
    var filteredRecipes: [Recipe] = [] {
        didSet {
            collectionView?.reloadData()
        }
    }
    /// True if user is searching
    var isSearching: Bool = false
    
    init(collection: RecipeCollection, collectionView: UICollectionView) {
        selectedCollection = collection
        self.collectionView = collectionView
        
        // Sort them alphabetically
        selectedCollection.recipes?.sort(by: { (recipe1, recipe2) -> Bool in
            return recipe1.title < recipe2.title
        })
        super.init()
    }
    
    // MARK: Searchbar delegate methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        isSearching = true
        searchRecipes(searchTerm: searchText)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        filteredRecipes.removeAll()
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /// Does the actual search, searches through each recipes' title and descriptionText, you can also add to include searching in the steps or else
    func searchRecipes(searchTerm: String) {
        guard let recipes = selectedCollection.recipes else {
            return
        }
        
        filteredRecipes = recipes.filter({ (recipe) -> Bool in
            if recipe.title.range(of: searchTerm) != nil {
                return true
            }
            
            if recipe.descriptionText.range(of: searchTerm) != nil {
                return true
            }
            
            return false
        })
    }
    
    /// Returns the right array to use, depending on use is searching/filtering
    func objectsToDisplay() -> [Recipe] {
        return isSearching ? filteredRecipes : selectedCollection.recipes!
    }
    
    /// Returns the right Recipe at the given indexPath
    func objectAtIndexPath(indexPath: IndexPath) -> Recipe {
        let objects = objectsToDisplay()
        return objects[indexPath.item]
    }
    
    // MARK: CollectionView datasource methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objectsToDisplay().count
    }
    
    /**
     Asks your data source object for the cell that corresponds to the specified item in the collection view.
     
     - parameter collectionView: The collection view requesting this information.
     - parameter indexPath:      The index path that specifies the location of the item.
     
     - returns: A configured cell object.
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RecipeCollectionViewCell
        
        let recipe = objectAtIndexPath(indexPath: indexPath)
        
        cell.titleLabel.text = recipe.title
        if let imageURL = recipe.imageURL() {
            if useLocalStore {
                cell.imageView.hnk_setImage(fromFile: recipe.mainImagePath)
            } else {
                cell.imageView.hnk_setImage(from: imageURL as URL!)
            }
        }
        
        return cell
    }
    
    deinit {
        selectedCollection = nil
    }
    
}
