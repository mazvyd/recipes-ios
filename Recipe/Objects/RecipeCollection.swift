//
//  RecipeCollection.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom object that displays a collection of recipes
class RecipeCollection: NSObject {

    /// File path to the collection object
    var folderPath: String?
    /// File path to the image of the collection
    var imageURL: String?
    /// Array of Recipe objects
    var recipes: [Recipe]?
    /// Name of the Collection
    var name: String?

    override init() {
        super.init()
    }

    /**
     Initialise the collection with the path of the image to be displayed as its main image, and the recipes

     - parameter path:    file path to the main image
     - parameter recipes: array of recipes inside the collection's folder
     - parameter folderPath: file path to the folder
     */
    init(withImagePath path: String, folderPath: String, recipes: [Recipe]) {
        imageURL = path
        self.folderPath = folderPath

        // Sort the recipes alphabetically
        self.recipes = recipes.sorted { (recipe1, recipe2) -> Bool in
            return recipe1.title.localizedCaseInsensitiveCompare(recipe2.title) == ComparisonResult.orderedAscending
        }
        super.init()
    }

    /**
     Returns a formatted name for the collection. It gets rid of the full path of the folder and only uses the last component.
     E.g.: Your folder's path might look like this .../vknfbdknjfd/nkjnbfdkj/fblfkn/Cookies, this way it uses only the last part the "Cookies" as the name

     - returns: Formatted name of the folder
     */
    func displayName() -> String {
        if let folderPath = self.folderPath as NSString? {
            return folderPath.lastPathComponent
        } else {
            return name ?? ""
        }
    }

    func formattedNumberOfRecipes() -> String {
        guard let recipes = recipes else {
            return ""
        }

        return "\(recipes.count) receptai"
    }
}
