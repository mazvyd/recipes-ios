//
//  Recipe.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/**
 *  Keys to save/load the recipe objects
 */
struct PropertyKey {
    static let titleKey = "title"
    static let difficultyKey = "difficulty"
    static let mainImagePathKey = "mainImagePath"
    static let prepTimeKey = "prepTimePath"
    static let portionsKey = "portions"
    static let descriptionKey = "descriptionText"
    static let ingredientsKey = "ingredients"
    static let cookingStepsKey = "cookingSteps"
}

/// Object that holds all the properties of a Recipe
class Recipe: NSObject, NSCoding {

    /// Path of the corresponding json file
    var jsonFilePath: String?
    /// Path of the main image
    var mainImagePath: String?

    /// Title
    var title: String = ""
    /// Difficulty level of the Recipe
    var difficulty: String = ""
    /// Preparation time
    var preparationTime: String = ""
    /// How many portions it serves
    var portions: String = ""
    /// Description of the Recipe
    var descriptionText: String = ""
    /// Ingredients it needs
    var ingredients: [Ingredient] = [Ingredient]()
    /// Steps of the Recipe
    var cookingSteps: [CookingStep] = [CookingStep]()

    override init() {
        super.init()
    }

    /**
     Initialises the Recipe object with the filePath of the JSON file and the mainImage

     - parameter jsonFilePath:  file path where the JSON file is located
     - parameter mainImagePath: file path to the main Image of the Recipe
     */
    init(jsonFilePath: String, mainImagePath: String) {
        self.jsonFilePath = jsonFilePath
        self.mainImagePath = mainImagePath
        super.init()

        parseJSONFile()
    }

    /**
     Initialise a Recipe with the given values

     - parameter mainImagePath:   image path
     - parameter title:           title
     - parameter difficulty:      difficulty
     - parameter preparationTime: prep time
     - parameter portions:        portions
     - parameter descriptionText: description
     - parameter ingredients:     ingredients
     - parameter steps:           steps
     */
    init?(mainImagePath: String, title: String, difficulty: String, preparationTime: String, portions: String, descriptionText: String, ingredients: [Ingredient], steps: [CookingStep]) {
        self.title = title
        self.mainImagePath = mainImagePath
        self.difficulty = difficulty
        self.preparationTime = preparationTime
        self.portions = portions
        self.descriptionText = descriptionText
        self.ingredients = ingredients
        self.cookingSteps = steps

        super.init()
    }

    /**
     Returns an object initialized from data in a given unarchiver.

     - parameter aDecoder: An unarchiver object.
     */
    required convenience init?(coder aDecoder: NSCoder) {
        let title = aDecoder.decodeObject(forKey: PropertyKey.titleKey) as! String
        let mainImagePath = aDecoder.decodeObject(forKey: PropertyKey.mainImagePathKey) as! String
        let difficulty = aDecoder.decodeObject(forKey: PropertyKey.difficultyKey) as! String
        let prepTime = aDecoder.decodeObject(forKey: PropertyKey.prepTimeKey) as! String
        let portions = aDecoder.decodeObject(forKey: PropertyKey.portionsKey) as! String
        let descriptionText = aDecoder.decodeObject(forKey: PropertyKey.descriptionKey) as! String
        let ingredients = aDecoder.decodeObject(forKey: PropertyKey.ingredientsKey) as! [Ingredient]
        let steps = aDecoder.decodeObject(forKey: PropertyKey.cookingStepsKey) as! [CookingStep]

        self.init(mainImagePath: mainImagePath, title: title, difficulty: difficulty, preparationTime: prepTime, portions: portions, descriptionText: descriptionText, ingredients: ingredients, steps: steps)!
    }

    /**
     Parses the json file at the filePath and creates all properties for the object
     */
    private func parseJSONFile() {
        do {
            let jsonData = try Data(contentsOf: URL(fileURLWithPath: jsonFilePath!))
            do {
                let jsonResult: NSDictionary = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                title = jsonResult["title"] as! String
                difficulty = jsonResult["difficulty"] as! String
                preparationTime = jsonResult["preparationTime"] as! String
                portions = jsonResult["portions"] as! String
                descriptionText = jsonResult["descriptionText"] as! String

                let ingredientsArray = jsonResult["ingredients"] as! [[String: AnyObject]]
                for ingredient in ingredientsArray {
                    ingredients.append(Ingredient(objectId: ingredient["id"] as! String, value: ingredient["value"] as! String))
                }

                let stepsArray = jsonResult["steps"] as! [[String: AnyObject]]
                for step in stepsArray {
                    cookingSteps.append(CookingStep(number: step["number"] as! NSNumber, step: step["value"] as! String))
                }
            } catch let error as NSError {
                print("Error occured: \(error.localizedDescription)")
            }
        } catch let error as NSError {
            print("Error occured: \(error.localizedDescription)")
        }
    }

    func imageURL() -> NSURL? {
        if useLocalStore == true {
            return NSURL(fileURLWithPath: mainImagePath!)
        } else {
            return NSURL(string: mainImagePath!)
        }
    }

    override func isEqual(_ object: Any?) -> Bool {
        if let otherObj = object as? Recipe {
            return otherObj.title == self.title
        }

        return false
    }

    /**
     Encodes the receiver using a given archiver.

     - parameter aCoder: An archiver object.
     */
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: PropertyKey.titleKey)
        aCoder.encode(mainImagePath, forKey: PropertyKey.mainImagePathKey)
        aCoder.encode(difficulty, forKey: PropertyKey.difficultyKey)
        aCoder.encode(preparationTime, forKey: PropertyKey.prepTimeKey)
        aCoder.encode(portions, forKey: PropertyKey.portionsKey)
        aCoder.encode(descriptionText, forKey: PropertyKey.descriptionKey)
        aCoder.encode(ingredients, forKey: PropertyKey.ingredientsKey)
        aCoder.encode(cookingSteps, forKey: PropertyKey.cookingStepsKey)
    }
}
