//
//  ProductsListFlowLayout.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom collectionView layout that displays the items in a list layout
class CollectionsListFlowLayout: UICollectionViewFlowLayout {

    /// Height of an item
    let itemHeight: CGFloat = 150

    /**
     Init

     - returns: self
     */
    override init() {
        super.init()
        setupLayout()
    }

    /**
     Init method

     - parameter aDecoder: aDecoder

     - returns: self
     */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }

    /**
     Sets up the layout for the collectionView. 0 distance between each cell, and vertical layout
     */
    func setupLayout() {
        minimumInteritemSpacing = 0
        minimumLineSpacing = 1
        scrollDirection = .vertical
    }

    /**
     Width of a single item

     - returns: width
     */
    func itemWidth() -> CGFloat {
        return collectionView!.frame.width
    }

    /// Size of the cells
    override var itemSize: CGSize {
        set {
            self.itemSize = CGSize(width: itemWidth(), height: itemHeight)
        }
        get {
            return CGSize(width: itemWidth(), height: itemHeight)
        }
    }

    /**
     Returns the content offset to use after an animated layout update or change.

     - parameter proposedContentOffset: The proposed point (in the coordinate space of the collection view’s content view) for the upper-left corner of the visible content.

     - returns: The content offset that you want to use instead.
     */
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
}
