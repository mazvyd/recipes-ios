//
//  RecipesTransitionPresenter.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

class RecipesTransitionPresenter: NSObject, UIViewControllerAnimatedTransitioning {

    /**
     The duration of the animation.

     :param: transitionContext the transition context object

     :returns: the duration
     */
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }

    /**
     The main animation happens here. This is called when the user selects a cell at the tableView

     :param: transitionContext the transition context object
     */
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        /// Get all the related controllers and containers
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)! as! RecipeCollectionViewController
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)! as! RecipesViewController
        let containerView = transitionContext.containerView


        let selectedIndexPaths = fromViewController.collectionView.indexPathsForSelectedItems
        if let selectedIndexPaths = selectedIndexPaths {
            let indexPath = selectedIndexPaths.first
            if indexPath != nil {
                let selectedCell = fromViewController.collectionView.cellForItem(at: indexPath!) as! CollectionViewCell
                let cellImageSnapshot = selectedCell.imageView.copyView()

                selectedCell.imageView.isHidden = true

                /// adjust the frame of the detail viewController's view
                toViewController.view.alpha = 0.0
                toViewController.collectionImageView.isHidden = true
                toViewController.collectionImageView.image = selectedCell.imageView.image

                /// Add it to the containerView
                containerView.addSubview(toViewController.view)

                // needed to get a nice transition between the cell and the view, since the thumbnailView is not fully visible in the tableView
                // create a new view that adds the cellImageSnapshot view as a subview
                let imageViewWrapperView = UIView(frame: containerView.convert(selectedCell.imageView.frame, from: selectedCell.imageView.superview))
                imageViewWrapperView.clipsToBounds = true
                imageViewWrapperView.addSubview(cellImageSnapshot)
                containerView.addSubview(imageViewWrapperView)

                /// Insert a white view behind the moving imageView to hide all the UI elements
                let whiteView = UIView(frame: fromViewController.view.frame)
                whiteView.backgroundColor = toViewController.view.backgroundColor
                containerView.insertSubview(whiteView, belowSubview: imageViewWrapperView)

                /**
                 *  Start the animation
                 */
                UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, options: .curveEaseOut, animations: {
                    toViewController.view.alpha = 1.0

                    // set the frame of the wrapperView to be the frame of destination viewControllers imageView (newsImageView), so it aligns perfectly
                    let frame = containerView.convert(CGRect(x: 0.0, y: 0.0, width: toViewController.view.frame.width, height: toViewController.collectionImageView.frame.height), from: toViewController.view)
                    imageViewWrapperView.frame.origin.y = 0.0
                    imageViewWrapperView.frame = frame
                }, completion: { (finished) in
                    // finish the animation by removing all the temporary views
                    toViewController.collectionImageView.isHidden = false
                    selectedCell.isHidden = false
                    whiteView.removeFromSuperview()
                    imageViewWrapperView.removeFromSuperview()
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                })
            }
        }
    }

}
