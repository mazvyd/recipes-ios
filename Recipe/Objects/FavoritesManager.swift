//
//  FavoritesManager.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import Foundation

/// Manager object that lets you save and load favorite recipes
class FavoritesManager: NSObject {
    
    /// Array of Recipes
    var favoriteRecipesArray: [Recipe] = []
    
    /// Reference to the Documents folder
    static let DocumentsDirectory: URL = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    /// Reference to the Favorites folder where the favorited Recipes will be held
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("Favorites", isDirectory: true)
    
    /// Makes the FavoritesManager a singleton
    static let sharedInstance: FavoritesManager = FavoritesManager()
    
    /**
     Check if a given Recipe is a favorite already
     
     - parameter recipe: recipe to check
     
     - returns: true if it is a favorite already
     */
    func isRecipeFavorite(recipe: Recipe) -> Bool {
        return favoriteRecipesArray.contains(recipe)
    }
    
    /**
     Returns the favorited recipe at a given indexPath
     
     - parameter indexPath: indexPath to use
     
     - returns: Recipe that was on that indexPath
     */
    func favoriteRecipeAtIndexPath(indexPath: NSIndexPath) -> Recipe {
        return favoriteRecipesArray[indexPath.row]
    }
    
    /**
     Save the given Recipe as a favorite
     
     - parameter recipe: Recipe object to save
     */
    func saveAsFavorite(recipe: Recipe) {
        favoriteRecipesArray.append(recipe)
    }
    
    /**
     Remove a selected recipe from favorites
     
     - parameter recipe: recipe to remove
     */
    func removeFromFavorites(recipe: Recipe) {
        let index = favoriteRecipesArray.index(of: recipe)
        if let index = index {
            favoriteRecipesArray.remove(at: index)
        }
    }
    
    /**
     Removes multiple recipes from the array of saved recipe
     
     - parameter recipes: recipes to remove
     */
    func removeRecipes(recipes: [Recipe]) {
        for recipe in recipes {
            removeFromFavorites(recipe: recipe)
        }
    }
    
}

extension FavoritesManager {
    
    /**
     Save the selected favorites to Disk to be able to load them next time
     */
    func saveFavoritesToDisk() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(favoriteRecipesArray, toFile: FavoritesManager.ArchiveURL.path)
        if !isSuccessfulSave {
            print("Failed to save recipes...")
        }
    }
    
    /**
     Read the favorite objects from Disk
     */
    func readFavoritesFromDisk() {
        if let results = NSKeyedUnarchiver.unarchiveObject(withFile: FavoritesManager.ArchiveURL.path) as? [Recipe] {
            favoriteRecipesArray = results
        }
    }
}


