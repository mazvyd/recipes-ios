//
//  BackendlessDownloader.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom object that lets you download all your recipes from your backendless.com app, or from other sources
class BackendlessDownloader: NSObject {

    let dataStore = Backendless.sharedInstance().persistenceService.of(RecipeCollection.self)

    /**
     Downloads all collections from the server and returns them in a completion block

     - parameter completion: Called after the successful request, returning all collections if any, error message if any
     */
    func downloadCollections(completion: @escaping (_ collections: [RecipeCollection]?, _ errorMessage: String?) -> ()) {
        let query = BackendlessDataQuery()

        let queryOptions = QueryOptions()
        queryOptions.sortBy = ["name"] // Sort them by name
        // If you wish to include subentities with your request, in this case, we wish to download Recipes as well that are related to Collections
        queryOptions.related = ["recipes", "recipes.cookingSteps", "recipes.ingredients"]
        query.queryOptions = queryOptions

        dataStore?.find(query, response: { (response) -> Void in
            completion(response?.data as? [RecipeCollection], nil)
        }) { (error) -> Void in
            completion(nil, error?.description)
        }
    }

}
