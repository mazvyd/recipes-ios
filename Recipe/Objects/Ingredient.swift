//
//  Ingredient.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import Foundation

/// Single ingredient object of a Recipe
class Ingredient: NSObject, NSCoding {
    
    /// ID of the object
    var objectId: String!
    /// Title of the Ingredient
    var value: String!

    override init() {
        super.init()
    }

    /**
     Initialise the Ingredient object with the properties

     - parameter objectID: id of the object
     - parameter value:   value
     */
    init(objectId: String, value: String) {
        self.objectId = objectId
        self.value = value
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        self.objectId = aDecoder.decodeObject(forKey: "objID") as! String
        self.value = aDecoder.decodeObject(forKey: "value") as! String
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(objectId, forKey: "objID")
        aCoder.encode(value, forKey: "value")
    }
}
