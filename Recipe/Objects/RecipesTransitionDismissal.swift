//
//  RecipesTransitionDismissal.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

class RecipesTransitionDismissal: NSObject, UIViewControllerAnimatedTransitioning {

    /**
     The duration of the animation.

     :param: transitionContext the transition context object

     :returns: the duration
     */
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }

    /**
     The main animation happens here. This is called when the user selects a dismisses the view.

     :param: transitionContext the transition context object
     */
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)! as! RecipesViewController
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)! as! RecipeCollectionViewController
        let containerView = transitionContext.containerView

        // create a snapshot of the newsImageView
        let imageSnapshotView = fromViewController.collectionImageView.copyView()
        imageSnapshotView.frame = containerView.convert(fromViewController.collectionImageView.frame, from: fromViewController.collectionImageView.superview)
        fromViewController.collectionImageView.isHidden = true

        if let selectedIndexPaths = toViewController.collectionView.indexPathsForSelectedItems {
            if let selectedIndexPath = selectedIndexPaths.first {
                let selectedCell = toViewController.collectionView.cellForItem(at: selectedIndexPath) as! CollectionViewCell
                selectedCell.imageView.isHidden = true

                toViewController.view.frame = transitionContext.finalFrame(for: toViewController)
                containerView.insertSubview(toViewController.view, belowSubview: fromViewController.view)

                // create a wrapper view for the snapshotView to be able to displace the thumbnailView inside the wrapperView just like how the cells are set up
                let imageSnapshotViewWrapperView = UIView(frame: containerView.convert(fromViewController.collectionImageView.frame, from: fromViewController.collectionImageView.superview))
                imageSnapshotViewWrapperView.clipsToBounds = true
                imageSnapshotViewWrapperView.addSubview(imageSnapshotView)

                containerView.addSubview(imageSnapshotViewWrapperView)

                // add a full view with white background to cover up the UI elements while the animation is going
                let whiteView = UIView(frame: fromViewController.view.frame)
                whiteView.backgroundColor = fromViewController.view.backgroundColor
                containerView.insertSubview(whiteView, belowSubview:imageSnapshotViewWrapperView)

                // start the animation
                UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: { () -> Void in
                    fromViewController.view.alpha = 0.0
                    // adjust the frame of the wrapperView
                    imageSnapshotViewWrapperView.frame = containerView.convert(selectedCell.imageView.frame,
                                                                               from:selectedCell.imageView.superview)

                    var newFrame = imageSnapshotView.frame
                    newFrame.origin.y = selectedCell.imageView.frame.minY
                    imageSnapshotView.frame = newFrame
                }) { (finished) -> Void in
                    // remove all the temporary views with a smooth animation
                    UIView.animate(withDuration: 0.2, animations: { () -> Void in
                        whiteView.removeFromSuperview()
                        imageSnapshotViewWrapperView.removeFromSuperview()
                        fromViewController.collectionImageView.isHidden = false
                        selectedCell.imageView.isHidden = false
                    })
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                }
            }
        }
    }
}
