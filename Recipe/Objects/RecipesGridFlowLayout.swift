//
//  RecipesGridFlowLayout.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom flow layout to displays the recipes in a grid layout
class RecipesGridFlowLayout: UICollectionViewFlowLayout {

    /// The height of each cell
    let itemHeight: CGFloat = 200

    /**
     Init

     */
    override init() {
        super.init()
        setupLayout()
    }

    /**
     Init method

     - parameter aDecoder: aDecoder

     - returns: self
     */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }

    /**
     Sets up the layout for the collectionView.
     */
    func setupLayout() {
        minimumInteritemSpacing = 10
        minimumLineSpacing = 5
        sectionInset = UIEdgeInsetsMake(8.0, 10.0, 50.0, 10.0)
        scrollDirection = .vertical
    }

    /**
     Width of the cell

     - returns: width
     */
    func itemWidth() -> CGFloat {
        return (collectionView!.frame.width/2)-15
    }

    /// Size of the cell
    override var itemSize: CGSize {
        set {
            self.itemSize = CGSize(width: itemWidth(), height: itemHeight)
        }
        get {
            return CGSize(width: itemWidth(), height: itemHeight)
        }
    }

    /**
     Returns the content offset to use after an animated layout update or change.

     - parameter proposedContentOffset: The proposed point (in the coordinate space of the collection view’s content view) for the upper-left corner of the visible content.

     - returns: The content offset that you want to use instead.
     */
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }

    /**
     Returns the starting layout information for an item being inserted into the collection view.

     - parameter itemIndexPath: The index path of the item being inserted. You can use this path to locate the item in the collection view’s data source.

     - returns: A layout attributes object that describes the position at which to place the corresponding cell.
     */
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = layoutAttributesForItem(at: itemIndexPath)
        if let attributes = attributes {
            attributes.alpha = 0.0

            if attributes.frame.origin.x < collectionView!.frame.width/2 {
                attributes.frame.origin.x = -100.0
            } else {
                attributes.frame.origin.x = collectionView!.frame.width+100
            }

            let size = collectionView!.frame.size
            attributes.center = CGPoint(x: size.width / 2.0, y: size.height / 2.0)
            return attributes
        }

        return nil
    }
}
