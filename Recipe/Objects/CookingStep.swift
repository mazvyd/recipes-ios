//
//  CookingStep.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import Foundation

/// A single step of a Recipe
class CookingStep: NSObject, NSCoding {

    /// Which step it is
    var number: NSNumber?
    /// Description of the Step
    var step: String?

    override init() {
        super.init()
    }

    /**
     Initialise the object with the given properties

     - parameter number: number
     - parameter step:   step
     */
    init(number: NSNumber, step: String) {
        self.number = number
        self.step = step
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        self.number = aDecoder.decodeObject(forKey: "number") as? NSNumber
        self.step = aDecoder.decodeObject(forKey: "step") as? String
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(number, forKey: "number")
        aCoder.encode(step, forKey: "step")
    }
}
