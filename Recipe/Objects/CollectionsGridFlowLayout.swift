//
//  ProductsGridFlowLayout.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom flow layout that displays the cells in a grid layout
class CollectionsGridFlowLayout: UICollectionViewFlowLayout {

    /// Height of the cells
    let itemHeight: CGFloat = 200

    /**
     Init

     - returns: self
     */
    override init() {
        super.init()
        setupLayout()
    }

    /**
     Init method

     - parameter aDecoder: aDecoder

     - returns: self
     */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }

    /**
     Sets up the layout for the collectionView. 1px distance between each cell, and vertical layout
     */
    func setupLayout() {
        minimumInteritemSpacing = 1
        minimumLineSpacing = 1
        scrollDirection = .vertical
    }

    /**
     Width of the cells

     - returns: width
     */
    func itemWidth() -> CGFloat {
        return (collectionView!.frame.width/2)-1
    }

    /// Size of the cells
    override var itemSize: CGSize {
        set {
            self.itemSize = CGSize(width: itemWidth(), height: itemHeight)
        }
        get {
            return CGSize(width: itemWidth(), height: itemHeight)
        }
    }

    /**
     Returns the content offset to use after an animated layout update or change.

     - parameter proposedContentOffset: The proposed point (in the coordinate space of the collection view’s content view) for the upper-left corner of the visible content.

     - returns: The content offset that you want to use instead.
     */
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
}
