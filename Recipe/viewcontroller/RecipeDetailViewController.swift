//
//  RecipeDetailViewController.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Recipe viewController to display the details of a recipe
class RecipeDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    /// TableView to display the recipe's details
    @IBOutlet weak var tableView: UITableView!
    /// Label to show the recipe's name/title
    @IBOutlet weak var recipeNameLabel: UILabel!
    /// ImageView to display the recipes main image
    @IBOutlet weak var recipeImageView: UIImageView!
    /// Button to favorite a recipe
    @IBOutlet weak var favoriteButton: UIButton!
    /// Button to dismiss the view
    @IBOutlet weak var backButton: UIButton!
    /// Custom view that holds the back, favorite button and title label
    @IBOutlet weak var navigationBarView: UIView!
    /// Custom view that displays the recipe details (portion, difficulty, time)
    @IBOutlet weak var recipesDetailWrapperView: UIView!
    /// Label to display the portion
    @IBOutlet weak var portionLabel: UILabel!
    /// Label to display the difficulty
    @IBOutlet weak var difficultyLabel: UILabel!
    /// Label that displays the time that required to make the recipe
    @IBOutlet weak var timeLabel: UILabel!
    /// Height constraint of the recipe image view
    @IBOutlet weak var recipeImageViewHeightConstraint: NSLayoutConstraint!
    
    /// To use white statusbar or not
    var useWhiteStatusbarColor: Bool = true
    /// Value to determine the initial size of the image
    var imageHeightConstant: CGFloat {
        get {
            if UIDevice.current.userInterfaceIdiom == .pad {
                return 250.0
            } else {
                return 140.0
            }
        }
    }
    
    /// True if the steps are displayed
    var stepsAreBeingDisplayed: Bool = true {
        didSet {
            if stepsAreBeingDisplayed {
                ingredientsButton.isSelected = false
                recipeButton.isSelected = true
            } else {
                ingredientsButton.isSelected = true
                recipeButton.isSelected = false
            }
        }
    }
    
    /// Button that shows the steps and details of the recipe when pressed
    @IBOutlet weak var recipeButton: RecipeDetailButton!
    /// Button that shows the ingredients when pressed
    @IBOutlet weak var ingredientsButton: RecipeDetailButton!
    
    /// Overlayview that is being displayed when the user saves a car to the garage
    lazy var overlayView: OverlayView = {
        let overlayView = OverlayView()
        return overlayView
    }()
    
    /// The recipe to display
    var recipeToDisplay: Recipe!
    
    /**
     Dismisses the current view
     */
    @IBAction func backButtonPressed() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    /**
     Called when the favorite button pressed, if the recipe is not a favorite yet, save it to the favorites
     */
    @IBAction func favoriteButtonPressed() {
        if FavoritesManager.sharedInstance.isRecipeFavorite(recipe: recipeToDisplay) {
            FavoritesManager.sharedInstance.removeFromFavorites(recipe: recipeToDisplay)
            favoriteButton.setImage(UIImage(named: "favoriteIcon"), for: .normal)
        } else {
            overlayView.displayView(onView: view)
            FavoritesManager.sharedInstance.saveAsFavorite(recipe: recipeToDisplay)
            favoriteButton.setImage(UIImage(named: "favoritedIcon"), for: .normal)
        }
    }
    
    /**
     Switch to the steps section
     */
    @IBAction func stepsSelectorPressed() {
        if stepsAreBeingDisplayed == false {
            stepsAreBeingDisplayed = true
            tableView.reloadData()
        }
    }
    
    /**
     Switch to the ingredients section
     */
    @IBAction func ingredientsSelectorPressed() {
        if stepsAreBeingDisplayed == true {
            stepsAreBeingDisplayed = false
            tableView.reloadData()
        }
    }
    
    /**
     Sets up the view after loading
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recipeNameLabel.text = recipeToDisplay.title
        if let imageURL = recipeToDisplay.imageURL() {
            if useLocalStore {
                recipeImageView.hnk_setImage(fromFile: recipeToDisplay.mainImagePath)
            } else {
                recipeImageView.hnk_setImage(from: imageURL as URL!)
            }
        }
        
        checkIfRecipeIsFavorite()
        
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.contentInset = UIEdgeInsetsMake(imageHeightConstant, 0.0, 0.0, 0.0)
        tableView.separatorColor = UIColor.rgbColor(red: 230.0, green: 218.0, blue: 224.0)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44.0
        
        navigationBarView.backgroundColor = .clear
        updateView()
        
        // add gradient view to the recipesDetailWrapperView
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0.0, y: 20.0, width: recipesDetailWrapperView.frame.width, height: recipesDetailWrapperView.frame.height-20.0)
        gradientLayer.colors = [UIColor.white.withAlphaComponent(0.0).cgColor, view.backgroundColor!.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.2)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1.0)
        recipesDetailWrapperView.layer.insertSublayer(gradientLayer, at: 0)
        
        stepsAreBeingDisplayed = true
        
        // Sort steps by their number value
        recipeToDisplay.cookingSteps.sort { (step1, step2) -> Bool in
            if let number1 = step1.number, let number2 = step2.number {
                return number1.doubleValue < number2.doubleValue
            }
            return false
        }
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    /**
     Update the view's labels with the recipe's details
     */
    func updateView() {
        difficultyLabel.text = recipeToDisplay.difficulty
        portionLabel.text = recipeToDisplay.portions
        timeLabel.text = recipeToDisplay.preparationTime
    }
    
    /**
     Adjust the navigation bar and the imageView when the scrollView (in this case the tableView) is scrolled
     
     - parameter scrollView: scrollview
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffsetValue = scrollView.contentOffset.y + imageHeightConstant
        if contentOffsetValue <= navigationBarView.frame.height {
            changeNavigationBarStyleToSolidColor(changeToSolidColor: false, alpha: 0)
        } else {
            let alpha = (contentOffsetValue/navigationBarView.frame.height)-1
            changeNavigationBarStyleToSolidColor(changeToSolidColor: true, alpha: alpha)
        }
        
        recipeImageViewHeightConstraint.constant = fabs(scrollView.contentOffset.y) + 60.0
        view.layoutIfNeeded()
    }
    
    /**
     Changes the navigation bar's color to a solid color
     
     - parameter changeToSolidColor: true to change it
     - parameter alpha:              alpha value to use
     */
    func changeNavigationBarStyleToSolidColor(changeToSolidColor: Bool, alpha: CGFloat) {
        if changeToSolidColor  {
            useWhiteStatusbarColor = false
            navigationBarView.backgroundColor = view.backgroundColor?.withAlphaComponent(alpha)
            UIView.animate(withDuration: 0.2, animations: {
                self.setNeedsStatusBarAppearanceUpdate()
                self.recipeNameLabel.textColor = globalTintColor
                self.favoriteButton.tintColor = globalTintColor
                self.backButton.tintColor = globalTintColor
            }, completion: { (finished) in
                if alpha >= 1.0 {
                    self.navigationBarView.layer.shadowOpacity = 1.0
                }
            })
        } else {
            useWhiteStatusbarColor = true
            navigationBarView.backgroundColor = .clear
            UIView.animate(withDuration: 0.2, animations: {
                self.setNeedsStatusBarAppearanceUpdate()
                self.recipeNameLabel.textColor = .white
                self.favoriteButton.tintColor = .white
                self.backButton.tintColor = .white
            }, completion: { (finished) in
                self.navigationBarView.layer.shadowOpacity = 0.0
            })
        }
    }
    
    /**
     Adds a shadow layer to the navigation bar
     */
    func addShadowLayerToNavigationBarView() {
        navigationBarView.layer.shadowColor = UIColor.black.cgColor
        navigationBarView.layer.shadowOpacity = 0.5
        navigationBarView.layer.shadowRadius = 2.0
        navigationBarView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    }
    
    /**
     Checks if the recipe is saved as a favorite
     */
    func checkIfRecipeIsFavorite() {
        if FavoritesManager.sharedInstance.isRecipeFavorite(recipe: recipeToDisplay) {
            favoriteButton.setImage(UIImage(named: "favoritedIcon"), for: .normal)
        }
    }
    
    // MARK: tableView methods
    /**
     Return the number of sections depending what is being displayed
     
     - parameter tableView: tableView
     
     - returns: number of sections
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        if stepsAreBeingDisplayed == true {
            return 2
        } else {
            return 1
        }
    }
    
    /**
     Return the number of rows to be displayed. If the steps are displayed, return 1 for the description and the number of steps.
     
     - parameter tableView: tableview
     - parameter section:   section
     
     - returns: number of rows
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if stepsAreBeingDisplayed == true {
            if section == 0 {
                return 1
            } else {
                return recipeToDisplay.cookingSteps.count
            }
        } else {
            return recipeToDisplay.ingredients.count
        }
    }
    
    /**
     Asks the data source for a cell to insert in a particular location of the table view.
     
     - parameter tableView: A table-view object requesting the cell.
     - parameter indexPath: An index path locating a row in tableView.
     
     - returns: configured cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // if the steps are selected, use those cell identifiers and configure them
        if stepsAreBeingDisplayed == true {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "descCell") as! RecipeDescriptionTableViewCell
                
                let style = NSMutableParagraphStyle()
                style.lineSpacing = 3.0
                style.alignment = .justified
                
                let attributedString = NSMutableAttributedString(string: recipeToDisplay.descriptionText, attributes: [NSForegroundColorAttributeName: cell.titleLabel.textColor, NSParagraphStyleAttributeName : style])
                
                cell.titleLabel.attributedText = attributedString
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "stepCell") as! RecipeStepTableViewCell
                
                let step = recipeToDisplay.cookingSteps[indexPath.row]
                cell.stepLabel.text = step.step
                cell.stepNumberLabel.text = "\(step.number ?? 0)"
                
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ingredientCell") as! RecipeIngredientTableViewCell
            
            let ingredient = recipeToDisplay.ingredients[indexPath.row]
            cell.titleLabel?.text = ingredient.value
            return cell
        }
    }
    
    /**
     Tells the delegate that the specified row is now selected. If not the steps are being displayed handle the selection and deselection of those
     
     - parameter tableView: A table-view object informing the delegate about the new row selection.
     - parameter indexPath: An index path locating the new selected row in tableView.
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    /**
     Use white statusbar by default
     
     - returns: white statusbar
     */
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return useWhiteStatusbarColor ? .lightContent : .default
    }
    
}
