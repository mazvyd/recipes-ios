//
//  FavoritesViewController.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom viewController that displays the Recipes that were saved as favorites
class FavoritesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    /// CollectionView to display the Recipes
    @IBOutlet weak var collectionView: UICollectionView!
    
    /// Array of favorite recipes
    var favoriteRecipes: [Recipe] {
        return FavoritesManager.sharedInstance.favoriteRecipesArray
    }
    
    /// Layout for the collectionView
    let gridLayout: RecipesGridFlowLayout = RecipesGridFlowLayout()
    
    /// Deletebutton to be shown when we are editing Recipes
    var deleteButton: UIBarButtonItem!
    
    /// An Empty view to be displayed when there are no saved Recipes
    lazy var emptyView: EmptyView = {
        let emptyView = EmptyView(frame: self.view.frame)
        return emptyView
    }()
    
    /// True if the statusbar needs to be hidden
    var hideStatusBar: Bool = false
    
    /// Array of Recipe objects we wish to remove
    var recipesToRemove: [Recipe] = [] {
        didSet {
            updateDeleteButtonTitle()
        }
    }
    
    /**
     If true, display the empty view
     
     - parameter display: boolean value that determines to display the empty view or not
     */
    func displayEmptyView(display: Bool) {
        if display == true {
            view.addSubview(emptyView)
            navigationController?.setNavigationBarHidden(true, animated: false)
            hideStatusBar = true
            setNeedsStatusBarAppearanceUpdate()
        } else {
            emptyView.removeFromSuperview()
            navigationController?.setNavigationBarHidden(false, animated: false)
            hideStatusBar = false
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    /**
     Updates the delete button's title with the amount of items to delete
     */
    func updateDeleteButtonTitle() {
        deleteButton.title = "Ištrinti (\(recipesToRemove.count))"
    }
    
    /**
     Removes all selected favorite recipes
     */
    func deleteButtonPressed() {
        if recipesToRemove.count > 0 {
            FavoritesManager.sharedInstance.removeRecipes(recipes: recipesToRemove)
            
            recipesToRemove.removeAll()
            collectionView.collectionViewLayout.invalidateLayout()
            collectionView.reloadData()
            updateDeleteButtonTitle()
            
            if favoriteRecipes.count == 0 {
                navigationItem.leftBarButtonItem = nil
                setEditing(false, animated: true)
                displayEmptyView(display: true)
            }
        }
    }
    
    // MARK: view methods
    /**
     Configure the view and the collectionView
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.allowsMultipleSelection = true
        collectionView.collectionViewLayout = gridLayout
        collectionView.reloadData()
        
        navigationItem.rightBarButtonItem = editButtonItem
        editButtonItem.title = "Redaguoti"
        navigationItem.title = "Mėgstamiausi"
        
        deleteButton = UIBarButtonItem(title: "Ištrinti", style: .plain, target: self, action: #selector(FavoritesViewController.deleteButtonPressed))
    }
    
    /**
     When the view appears, reload the collectionView
     
     - parameter animated: animated
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView.reloadData()
        displayEmptyView(display: favoriteRecipes.count == 0)
    }
    
    /**
     Sets whether the view controller shows an editable view.
     
     - parameter editing:  If true, the view controller should display an editable view; otherwise, false.
     - parameter animated: If true, animates the transition; otherwise, does not.
     */
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        if editing {
            navigationItem.leftBarButtonItem = deleteButton
            self.editButtonItem.title = "Išsaugoti"
        } else {
            navigationItem.leftBarButtonItem = nil
            self.editButtonItem.title = "Redaguoti"
        }
        
        recipesToRemove.removeAll()
        collectionView.reloadData()
    }
    
    // MARK: collectionView methods
    /**
     Tells the collectionView what cell to display and which object to use
     
     - parameter collectionView: collectionView
     - parameter indexPath:      indexPath
     
     - returns: configured cell
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RecipeCollectionViewCell
        
        let recipe = favoriteRecipes[indexPath.row]
        cell.titleLabel.text = recipe.title
        
        if let imageURL = recipe.imageURL() {
            if useLocalStore {
                cell.imageView.hnk_setImage(fromFile: recipe.mainImagePath)
            } else {
                cell.imageView.hnk_setImage(from: imageURL as URL!)
            }
        }
        
        cell.markToDelete(toDelete: recipesToRemove.contains(recipe))
        
        return cell
    }
    
    /**
     Number of items to display
     
     - parameter collectionView: collectionView
     - parameter section:        section
     
     - returns: number of items
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favoriteRecipes.count
    }
    
    /**
     Called when the cell is selected. If not editing, show the detail view of the Recipe, if editing add it or remove it from the array of recipes to delete
     
     - parameter collectionView: collectionview
     - parameter indexPath:      indexpath
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Select item
        let recipe = favoriteRecipes[indexPath.row]
        if isEditing {
            if recipesToRemove.contains(recipe) {
                let index = recipesToRemove.index(of: recipe)
                if let index = index {
                    recipesToRemove.remove(at: index)
                }
            } else {
                recipesToRemove.append(recipe)
            }
        } else {
            performSegue(withIdentifier: "showRecipe", sender: indexPath)
        }
        
        collectionView.reloadData()
    }
    
    /**
     When the cell is deselected, remove it from the array of objects we want to delete
     
     - parameter collectionView: collectionView
     - parameter indexPath:      selected indexPath
     */
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        // Deselect item
        let recipe = favoriteRecipes[indexPath.row]
        if isEditing {
            if recipesToRemove.contains(recipe) {
                let index = recipesToRemove.index(of: recipe)
                if let index = index {
                    recipesToRemove.remove(at: index)
                }
            }
        }
        
        collectionView.reloadData()
    }
    
    /**
     Notifies the view controller that a segue is about to be performed. Shows the detail view of a selected object
     
     - parameter segue:  The segue object containing information about the view controllers involved in the segue.
     - parameter sender: The object that initiated the segue. You might use this parameter to perform different actions based on which control (or other object) initiated the segue.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRecipe" {
            if let sender = sender as? NSIndexPath {
                let selectedRecipe = favoriteRecipes[sender.item]
                
                let destinationController = segue.destination as! RecipeDetailViewController
                destinationController.recipeToDisplay = selectedRecipe
            }
        }
    }
    
    /**
     Specifies whether the view controller prefers the status bar to be hidden or shown.
     true if the status bar should be hidden or false if it should be shown
     
     - returns: if the status bar should be hidden or NO if it should be shown.
     */
    override var prefersStatusBarHidden: Bool {
        return hideStatusBar
    }
}
