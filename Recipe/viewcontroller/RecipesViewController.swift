//
//  RecipesViewController.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// ViewController that displays all the selected recipes in a collectionView using a grid layout
class RecipesViewController: UIViewController, UICollectionViewDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate {

    /// CollectionView to display the recipes
    @IBOutlet weak var collectionView: UICollectionView!
    /// ImageView that displays the main image of the collection
    @IBOutlet weak var collectionImageView: UIImageView!
    /// Searchbar
    @IBOutlet weak var searchBar: UISearchBar!

    /// RecipeCollection object to be displayed
    var collectionToDisplay: RecipeCollection!
    /// Datasource object to manage the collectionView's data and searching
    var datasource: RecipesDatasource!
    /// Layout object that defines how to display the recipes
    let gridLayout: RecipesGridFlowLayout = RecipesGridFlowLayout()

    /// interactive transition object to be able to dismiss the controller while keeping the custom transition
    private var interactivePopTransition: UIPercentDrivenInteractiveTransition!

    // MARK: view methods
    /**
    Do some setup after the view is loaded
    */
    override func viewDidLoad() {
        super.viewDidLoad()

        title = collectionToDisplay.displayName()
        // To keep the swipe to go back style
        navigationController?.interactivePopGestureRecognizer?.delegate = self

        // gridLayout.sectionInset = UIEdgeInsetsMake(110.0, 10.0, 50.0, 10.0)
        collectionView.collectionViewLayout = gridLayout

        datasource = RecipesDatasource(collection: collectionToDisplay, collectionView: collectionView)
        collectionView.dataSource = datasource
        collectionView.reloadData()

        searchBar.backgroundColor = .clear
        searchBar.backgroundImage = UIImage()
        searchBar.isTranslucent = true
        searchBar.delegate = datasource

        collectionImageView.hnk_setImage(fromFile: collectionToDisplay.imageURL)

        // add gradient view to the collectionImageView
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0.0, y: collectionImageView.frame.height-50.0, width: collectionImageView.frame.width, height: 50.0)
        gradientLayer.colors = [UIColor.white.withAlphaComponent(0.0).cgColor, view.backgroundColor!.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1.0)
        collectionImageView.layer.addSublayer(gradientLayer)

        /// add an edge recognizer to be able to pop the view, when the user swipes from the left side of the screen
        let screenEdgePopRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(RecipesViewController.handlePopRecognizer(screenPopRecognizer:)))
        screenEdgePopRecognizer.edges = .left
        view.addGestureRecognizer(screenEdgePopRecognizer)
    }

    /**
     Set the navigation controller's delegate to this view

     :param: animated yes, if animated
     */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.delegate = self
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.delegate = nil
    }

    /**
     Change the look of the navigationBarView when the view is scrolled

     - parameter scrollView: scrollView
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > collectionImageView.frame.height {
            collectionImageView.alpha = 1.0 // 0.0 to make the image dissapear when scrolling
        } else {
            collectionImageView.alpha = 1.0
        }
    }

    /**
     Handles the screenPopRecognizer's gestures, when certain value reached, either finish the transition, or pop the viewcontroller

     :param: screenPopRecognizer the recognizer
     */
    func handlePopRecognizer(screenPopRecognizer: UIScreenEdgePanGestureRecognizer) {
        var progress = screenPopRecognizer.translation(in: view).x / view.frame.width
        progress = min(1.0, max(0.0, progress))

        switch screenPopRecognizer.state {

        case .began:
            interactivePopTransition = UIPercentDrivenInteractiveTransition()
            _ = navigationController?.popViewController(animated: true)
        case .changed:
            interactivePopTransition.update(progress)
        case .cancelled, .ended, .failed:
            if progress > 0.5 {
                interactivePopTransition.finish()
            }
            else {
                interactivePopTransition.cancel()
            }
            interactivePopTransition = nil
        default:
            _ = navigationController?.popViewController(animated: true)
        }
    }

    // MARK: animation handling
    /**
     Called to allow the delegate to return a noninteractive animator object for use during view controller transitions.

     :param: navigationController The navigation controller whose navigation stack is changing.
     :param: operation            The type of transition operation that is occurring.
     :param: fromVC               The currently visible view controller.
     :param: toVC                 The view controller that should be visible at the end of the transition.

     :returns: The animator object responsible for managing the transition animations, or nil if you want to use the standard navigation controller transitions.
     */
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if fromVC == self && toVC.isKind(of: RecipeCollectionViewController.self) {
            return nil // RecipesTransitionDismissal()
        } else {
            return nil
        }
    }

    /**
     Called to allow the delegate to return an interactive animator object for use during view controller transitions.

     :param: navigationController The navigation controller whose navigation stack is changing.
     :param: animationController The noninteractive animator object provided by the delegate’s navigationController:animationControllerForOperation:fromViewController:toViewController: method.

     :returns: The animator object responsible for managing the transition animations, or nil if you want to use the standard navigation controller transitions.
     */
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if animationController.isKind(of: RecipesTransitionDismissal.self) {
            return nil // interactivePopTransition
        } else {
            return nil
        }
    }

    // MARK: collectionView methods
    /**
     Perform the showRecipe segue when a cell is tapped

     - parameter collectionView: collectionView
     - parameter indexPath:      indexPath
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showRecipe", sender: indexPath)
    }

    /**
     Adjust the collectionView's layout

     - parameter size:        size
     - parameter coordinator: coordinator
     */
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        collectionView.collectionViewLayout.invalidateLayout()
    }

    // MARK: - Navigation
    /**
    Assign the selected recipe object when the segue is performed

    - parameter segue:  segue
    - parameter sender: sender
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRecipe", let indexPath = sender as? IndexPath {
            let selectedRecipe = datasource.objectAtIndexPath(indexPath: indexPath)

            let destinationController = segue.destination as! RecipeDetailViewController
            destinationController.recipeToDisplay = selectedRecipe
        }
    }

    /**
     Use white statusbar by default

     - returns: white statusbar
     */
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
