//
//  MainTabbarViewController.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

 /// The tabbar controller that hold the tabBar of the app.

class MainTabbarViewController: UITabBarController {

    /**
     Adds new tabbar items to the tabbar
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        customiseApp()
    }

    /**
     Customise the look of your iOS app
     */
    func customiseApp() {
        UITabBar.appearance().tintColor = tabbarIconTintColor
        UITabBar.appearance().barTintColor = tabbarBackgroundColor

        UINavigationBar.appearance().barTintColor = navigationBarColor
        UINavigationBar.appearance().tintColor = globalTintColor

        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: globalTintColor, NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)]
    }
    
}
