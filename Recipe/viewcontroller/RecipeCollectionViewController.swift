//
//  RecipeCollectionViewController.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit
import GoogleMobileAds

/// Custom viewController that displays all the recipeCollection objects
class RecipeCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UINavigationControllerDelegate {

    /// CollectionView that displays the objects
    @IBOutlet weak var collectionView: UICollectionView!
    /// Button to change the current layout
    @IBOutlet weak var layoutChangerButton: UIBarButtonItem!
    /// View to show Firebase/Admob banner
    @IBOutlet weak var bannerView: GADBannerView!

    @IBOutlet weak var bannerViewHeightConstraint: NSLayoutConstraint!
    /// For showing an interstitial banner ad
    var interstitial: GADInterstitial!

    /// Returns true if the current layout is a grid layout
    var isCurrentLayoutGrid: Bool = false
    /// Manager object that loads all the recipeCollections
    lazy var collectionManager = RecipeCollectionManager()

    /// Flow layout that displays the collections in a Grid layout
    let gridFlowLayout = CollectionsGridFlowLayout()
    /// Flow layout that displays the collections in a List layout, like in a tableView
    let listFlowLayout = CollectionsListFlowLayout()

    var refreshControl: UIRefreshControl!

    /**
     Change the current layout to a grid or list
     */
    @IBAction func changeLayout(button: UIBarButtonItem) {
        if isCurrentLayoutGrid {
            button.image = UIImage(named: "listIcon")
            changeLayoutToListView()
        } else {
            button.image = UIImage(named: "gridIcon")
            changeLayoutToGridView()
        }
    }

    /**
     When the view loads make some customisation
     */
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Receptai"
        // If using an iPad, use the grid layout and remove the layout changer button
        if UIDevice.current.userInterfaceIdiom == .pad {
            changeLayoutToGridView()
            navigationItem.rightBarButtonItem = nil
        } else {
            changeLayoutToListView()
        }

        // Setup pull to refresh
        if useLocalStore == false {
            refreshControl = UIRefreshControl()
            refreshControl.tintColor = .white
            refreshControl.addTarget(self, action: #selector(RecipeCollectionViewController.refreshView), for: .valueChanged)
            collectionView.addSubview(refreshControl)

            collectionView.setContentOffset(CGPoint(x: 0.0, y: -44.0), animated: false)
        }

        refreshView()
//        configureAdMob()
//        createAndLoadInterstitial()
    }

    func refreshView() {
        self.refreshControl?.beginRefreshing()
        
        collectionManager.readRecipeCollectionsFolder { (collections, error) in
            self.refreshControl?.endRefreshing()
            if let error = error {
                let alertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            } else {
                self.collectionView.reloadData()
            }
        }
    }

    /**
     Set the navigation controller's delegate to this controller and deselect a cell, if there is one selected

     :param: animated YES if to animate it
     */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.delegate = self

//        if interstitial.isReady {
//            interstitial.present(fromRootViewController: self)
//        }
    }

    /**
     Show the navigation bar when the view is about to be shown

     - parameter animated: true if animated
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    /**
     Set the navigation controller's delegate to nil when the view is about to disappear

     :param: animated YES if to animate it
     */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.delegate = nil
    }

    // MARK: collectionView methods
    /**
    Number of items to display

    - parameter collectionView: collectionview
    - parameter section:        section

    - returns: number of items
    */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionManager.recipeCollections().count
    }

    /**
     Asks your data source object for the cell that corresponds to the specified item in the collection view.

     - parameter collectionView: collectionview
     - parameter indexPath:      indexPath

     - returns: configured cell
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell

        let collection = collectionManager.recipeCollections()[indexPath.item]

        cell.titleLabel.text = collection.displayName()
        cell.detailLabel.text = collection.formattedNumberOfRecipes()

        // Check if we have saved the image before locally
        if let imagePath = collection.imageURL {
            if useLocalStore == true {
                cell.imageView.hnk_setImage(fromFile: imagePath)
            } else {
                cell.imageView.hnk_setImage(from: URL(string: imagePath))
            }
        }

        return cell
    }

    /**
     When a cell is selected perform the showRecipes segue to show the RecipesViewController

     - parameter collectionView: collectionView
     - parameter indexPath:      indexPath
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showRecipes", sender: indexPath)
    }

    /**
     Change the current layout to a list view
     */
    func changeLayoutToListView() {
        isCurrentLayoutGrid = false

        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.setCollectionViewLayout(listFlowLayout, animated: true)
    }

    /**
     Changes the current layout to be a grid
     */
    func changeLayoutToGridView() {
        isCurrentLayoutGrid = true

        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.setCollectionViewLayout(gridFlowLayout, animated: true)
    }

    /**
     Adjust the collectionView's layout

     - parameter size:        size
     - parameter coordinator: coordinator
     */
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        collectionView.collectionViewLayout.invalidateLayout()
    }

    // MARK: - Navigation
    /**
    Notifies the view controller that a segue is about to be performed. Show the RecipesViewController

    - parameter segue:  segue to perform
    - parameter sender: sender
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRecipes" {
            if let sender = sender as? NSIndexPath {
                let selectedCollection = collectionManager.recipeCollections()[sender.item]

                let destinationController = segue.destination as! RecipesViewController
                destinationController.collectionToDisplay = selectedCollection
            }
        }
    }

    /**
     Called to allow the delegate to return a noninteractive animator object for use during view controller transitions.

     :param: navigationController The navigation controller whose navigation stack is changing
     :param: operation            The type of transition operation that is occurring.
     :param: fromVC               The currently visible view controller.
     :param: toVC                 The view controller that should be visible at the end of the transition.

     :returns: The animator object responsible for managing the transition animations, or nil if you want to use the standard navigation controller transitions.
     */
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if fromVC == self && toVC.isKind(of: RecipesViewController.self) {
            return nil // RecipesTransitionPresenter() // Uncoment this if need an animation
        } else {
            return nil
        }
    }

}

// MARK: AdMob methods
extension RecipeCollectionViewController {

    /// Configures AdMob bannerView to be displayed at the bottom
    func configureAdMob() {
        bannerView.adUnitID = adMobAdUnitID
        bannerView.rootViewController = self
        if UIDevice.current.userInterfaceIdiom == .pad {
            bannerViewHeightConstraint.constant = 90
        } else {
            bannerViewHeightConstraint.constant = 50
        }
        bannerView.adSize = kGADAdSizeSmartBannerPortrait
        bannerView.load(GADRequest())

        bannerView.layoutIfNeeded()
    }

    /// Creates and loads an interstitial ad
    func createAndLoadInterstitial() {
        interstitial = GADInterstitial(adUnitID: adMobInterstitialAdUnitID)

        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        interstitial.load(request)
    }

}
