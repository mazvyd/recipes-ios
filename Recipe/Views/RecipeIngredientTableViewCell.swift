//
//  RecipeIngredientTableViewCell.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom tableViewCell that displays an ingredient of a Recipe
class RecipeIngredientTableViewCell: UITableViewCell {

    /// Label to display the title of the ingredient
    @IBOutlet weak var titleLabel: UILabel!

    /**
     Make some customisation
     */
    override func awakeFromNib() {
        super.awakeFromNib()

        titleLabel.textColor = UIColor.rgbColor(red: 105.0, green: 72.0, blue: 85.0)
    }

}
