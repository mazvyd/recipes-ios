//
//  RecipeDescriptionTableViewCell.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Cell to display the description of a recipe
class RecipeDescriptionTableViewCell: UITableViewCell {

    /// Label to display the description of a cell
    @IBOutlet weak var titleLabel: UILabel!

    /**
     Initial setup, to have edge-to-edge separator
     */
    override func awakeFromNib() {
        super.awakeFromNib()
        layoutMargins = .zero
        separatorInset = .zero
    }

}
