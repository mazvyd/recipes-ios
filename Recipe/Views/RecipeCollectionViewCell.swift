//
//  RecipeCollectionViewCell.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom UICollectionViewCell to display the Recipe object
class RecipeCollectionViewCell: UICollectionViewCell {

    /// Imageview to display the image of the Recipe
    @IBOutlet weak var imageView: UIImageView!
    /// Label to display the title of the Recipe
    @IBOutlet weak var titleLabel: UILabel!
    /// Marks the recipe to be deleted
    @IBOutlet weak var deleteIconView: UIImageView!

    /**
     Add a bit of corner radius to the imageView
     */
    override func awakeFromNib() {
        super.awakeFromNib()

        imageView.layer.cornerRadius = 2.0
    }

    /**
     Reset the imageView's image property
     */
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }

    /**
     Show a different state if the cell is being marked as delete

     - parameter delete: true if the cell needs to be deleted
     */
    func markToDelete(toDelete: Bool) {
        deleteIconView.alpha = toDelete == true ? 1.0 : 0.0
        titleLabel.alpha = toDelete == true ? 0.7 : 1.0
    }
}
