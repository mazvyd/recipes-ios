//
//  RecipeDetailButton.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom button at RecipeDetailViewController
class RecipeDetailButton: UIButton {

    /// Show a different title color if the button is selected
    override var isSelected: Bool {
        didSet {
            if isSelected == true {
                self.setTitleColor(UIColor.rgbColor(red: 234.0, green: 41.0, blue: 125.0), for: [])
            } else {
                self.setTitleColor(UIColor.rgbColor(red: 234.0, green: 41.0, blue: 125.0).withAlphaComponent(0.4), for: [])
            }
        }
    }

    /**
     Set the titleColor by default
     */
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitleColor(UIColor.rgbColor(red: 234.0, green: 41.0, blue: 125.0), for: [])
    }
}
