//
//  CollectionViewCell.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom UICollectionViewCell to display the Recipe collection object
class CollectionViewCell: UICollectionViewCell {

    /// Imageview to display the image of the Recipe Collection
    @IBOutlet weak var imageView: UIImageView!
    /// Label to display the title of the Recipe Collection
    @IBOutlet weak var titleLabel: UILabel!
    /// Label to display how many recipes are inside
    @IBOutlet weak var detailLabel: UILabel!

    /**
     Reset the imageView's image property
     */
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }

}
