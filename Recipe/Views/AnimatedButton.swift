//
//  AnimatedButton.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

// MARK: - Extension for UIButtons
extension UIButton {

    /**
     Animate the button when it is pressed.

     - parameter touches: touches
     - parameter event:   event
     */
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.imageView!.layer.transform = CATransform3DMakeScale(1.3, 1.3, 1.0)
            }) { (finished) -> Void in
                self.imageView!.layer.transform = CATransform3DIdentity
        }
    }

}
