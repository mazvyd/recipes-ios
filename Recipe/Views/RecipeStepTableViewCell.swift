//
//  RecipeStepTableViewCell.swift
//  Recipe
//
//  Created by Mazvydas Bartasius on 01/02/2017.
//  Copyright © 2017 Mazvydas Bartasius. All rights reserved.
//

import UIKit

/// Custom TableViewCell that display a step of a Recipe
class RecipeStepTableViewCell: UITableViewCell {

    /// Label to display the step of the recipe
    @IBOutlet weak var stepLabel: UILabel!
    /// Label to display the position of the step
    @IBOutlet weak var stepNumberLabel: UILabel!
    /// View that holds the stepNumberLabel inside
    @IBOutlet weak var stepCountView: UIView!

    /**
     Customise the stepCountView to be a circle
     */
    override func awakeFromNib() {
        super.awakeFromNib()
        stepCountView.layer.cornerRadius = stepCountView.frame.width/2
    }

}
